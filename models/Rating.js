var mongoose = require('mongoose');
require('mongoose-double')(mongoose);
var SchemaTypes = mongoose.Schema.Types;

var ratingSchema = new mongoose.Schema({
        userId: {
            type: Number,
            required: true
        },
        songId: {
            type: Number,
            required: true
        },
        rating: {
            type: Number,
            enum: [1,2,3,4,5],
            required: true
        }
    }, {
    timestamps: true
});

module.exports = mongoose.model('Rating', ratingSchema);