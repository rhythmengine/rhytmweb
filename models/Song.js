var mongoose = require('mongoose');
require('mongoose-double')(mongoose);
var SchemaTypes = mongoose.Schema.Types;

var songsSchema = new mongoose.Schema({
        id: {
            type: Number,
            required: true
        },
        song_name: {
            type: String,
            required: true
        },
        artist_name: {
            type: String,
            required: true
        },
        album_names: {
            type: String,
            required: true
        },
        playlist: {
            type: String,
            required: true
        },
        song_popularity: {
            type: Number,
            required: true
        },
        song_duration_ms: {
            type: Number,
            required: true
        },
        acousticness: {
            type: SchemaTypes.Double,
            required: true
        },
        danceability: {
            type: SchemaTypes.Double,
            required: true
        },
        energy: {
            type: SchemaTypes.Double,
            required: true
        },
        instrumentalness: {
            type: SchemaTypes.Double,
            required: true
        },
        key: {
            type: Number,
            required: true
        },
        liveness: {
            type: SchemaTypes.Double,
            required: true
        },
        loudness: {
            type: SchemaTypes.Double,
            required: true
        },
        audio_mode: {
            type: Number,
            required: true
        },
        speechiness: {
            type: SchemaTypes.Double,
            required: true
        },
        tempo: {
            type: SchemaTypes.Double,
            required: true
        },
        time_signature: {
            type: Number,
            required: true
        },
        audio_valence: {
            type: SchemaTypes.Double,
            required: true
        }
    }, {
    timestamps: true
});

module.exports = mongoose.model('Song', songsSchema);