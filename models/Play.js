var mongoose = require('mongoose');
require('mongoose-double')(mongoose);
var SchemaTypes = mongoose.Schema.Types;

var playsSchema = new mongoose.Schema({
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        songId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Song',
            required: true
        },
        plays: {
            type: Number,
            required: true
        }
    }, {
    timestamps: true
});

module.exports = mongoose.model('Play', playsSchema);