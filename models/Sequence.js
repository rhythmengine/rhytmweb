var mongoose = require('mongoose');
require('mongoose-double')(mongoose);

var sequenceSchema = new mongoose.Schema({
        reference: {
            type: String,
            required: true
        },
        count: {
            type: Number,
            default: 0,
            required: true
        }
    }, {
    timestamps: true
});

module.exports = mongoose.model('Sequence', sequenceSchema);