var Song = require('../models/Song');
var Play = require('../models/Play');
var Rating = require('../models/Rating');
var User = require('../models/User');
var Sequence = require('../models/Sequence');
var request = require('request')

module.exports.getSongs = function(req, res, next){
	var keyword = decodeURI(req.body.keyword)
	Song.find({$or: [{'song_name' : { $regex: req.query.keyword, $options: 'i' }}, {'album_name' : { $regex: req.query.keyword, $options: 'i' }}, {'artist_name' : { $regex: req.query.keyword, $options: 'i' }}]}, function(err, result){
		res.json({
			status: 'success',
			message: result
		});
	});
}

module.exports.similarSongs = function(req, res, next){
	request('http://127.0.0.1:5000/similarsongs/'+req.query.id, function (error, response, body) {
		var jsonObject = JSON.parse(response.body);
		console.log(jsonObject)
		Song.find({'id': {$in: jsonObject}}, function(err, result){
			console.log(result)
			res.json({
				message: result
			})
		})
	});
}

module.exports.songsFromAlbum = function(req, res, next){
	var album = req.query.album;
	Song.find({album_names: album}, function(err, songs){
		if(err){
			res.json({
				status:'error',
				message: err
			});
		}else{
			res.json({
				status:'success',
				message: songs
			});
		}
	});		
}

module.exports.songsFromArtist = function(req,res,next){
	var artist = req.query.artist;
	Song.find({artist_name: artist}, function(err, songs){
		if(err){
			res.json({
				status:'error',
				message: err,  
			});
		}else if(songs.length==0){
			res.json({
				status:'error',
				message: 'No songs with given artist name found',
			});
		}else{
			res.json({
				status:'success',
				message: songs,
	 		})
		}
	});
}

module.exports.songPlayed = function(req,res,next){
	var song = req.body.song_id;
	var user = req.body.user_id;

	Play.find({'userId': req.body.user_id, 'songId': req.body.song_id}, function(err, result){
		if(err){
			res.json({
				status:'error',
				message: err,  
			});
		}else if(result.length == 0){
			var play = {
				userId: req.body.user_id,
				songId: req.body.song_id,
				plays: 1
			};

			Play.create(play, function(err, play){
				if(err){
					res.json({
						status:'error',
						message: err,  
					});	
				}else{
					res.json({
						status:'success',
						message: 'Registered',  
					});
				}
			})
		}else if(result.length > 0){
			Play.findOneAndUpdate({'userId':req.body.user_id, 'songId': req.body.song_id}, {$inc: {plays: 1}}, {new: true}, function(err, result){
				if(err){
					res.json({
						status:'error',
						message: err,  
					});
				}else{
					res.json({
						status:'success',
						message: 'Updated',  
					});	
				}
			});
		}
	})

}

module.exports.recentArtist = function(req,res,next){
	var user= req.query.userId;
	Play.find({'userId': user}).populate('songId').sort({'updatedAt':-1}).limit(5).exec(function(err, plays){
		if(err){
			res.json({
				status:'error',
				message: err,  
			});
		}else{
			var final = []
			Song.find({'artist_name':plays[0].songId.artist_name},function(err, songs1){
				if(err){
					res.json({
						status:'error',
						message: err,  
					})
				}else{
					var artist1 = {};
					artist1.name = plays[0].songId.artist_name
					artist1.songs = songs1
					final.push(artist1)
					Song.find({'artist_name':plays[1].songId.artist_name},function(err, songs2){
						if(err){
							res.json({
								status:'error',
								message: err,  
							})
						}else{
							var artist2 = {};
							artist2.name = plays[1].songId.artist_name
							artist2.songs = songs2
							final.push(artist2)
							Song.find({'artist_name':plays[2].songId.artist_name},function(err, songs3){
								if(err){
									res.json({
										status:'error',
										message: err,  
									});
								}else{
									var artist3 = {};
									artist3.name = plays[2].songId.artist_name
									artist3.songs = songs3
									final.push(artist3)
									Song.find({'artist_name':plays[3].songId.artist_name},function(err, songs4){
										if(err){
											res.json({
												status:'error',
												message: err,  
											});
										}else{
											var artist4 = {};
											artist4.name = plays[3].songId.artist_name
											artist4.songs = songs4
											final.push(artist4)
											res.json({
												status:'succ',
												message: final	,
											});	
										}
									})								
								}
							})	
						}
					})
				}
			})
		}
	})
}

module.exports.recentAlbum = function(req,res,next){
	var user= req.query.userId;
	Play.find({'userId': user}).populate('songId').sort({'updatedAt':-1}).limit(5).exec(function(err, plays){
		if(err){
			res.json({
				status:'error',
				message: err,  
			});
		}else{
			console.log(plays[0].songId.album_names)
			var final = []
			Song.find({'album_names':plays[0].songId.album_names},function(err, songs1){
				if(err){
					res.json({
						status:'error',
						message: err,  
					})
				}else{
					var album1 = {};
					album1.name = plays[0].songId.album_names
					album1.songs = songs1
					final.push(album1)
					Song.find({'album_names':plays[1].songId.album_names},function(err, songs2){
						if(err){
							res.json({
								status:'error',
								message: err,  
							})
						}else{
							var album2 = {};
							album2.name = plays[1].songId.album_names
							album2.songs = songs2
							final.push(album2)
							Song.find({'album_names':plays[2].songId.album_names},function(err, songs3){
								if(err){
									res.json({
										status:'error',
										message: err,  
									});
								}else{
									var album3 = {};
									album3.name = plays[2].songId.album_names
									album3.songs = songs3
									final.push(album3)
									Song.find({'album_names':plays[3].songId.album_names},function(err, songs4){
										if(err){
											res.json({
												status:'error',
												message: err,  
											});
										}else{
											var album4 = {};
											album4.name = plays[3].songId.album_names
											album4.songs = songs4
											final.push(album4)
											res.json({
												status:'succ',
												message: final	,
											});	
										}
									})								
								}
							})	
						}
					})
				}
			})
		}
	})
}

module.exports.rate = function(req, res, next){
	Rating.find({'userId': req.body.user_id, 'songId': req.body.song_id}, function(err, result){
		if(err){
			res.json({
				status:'error',
				message: err,  
			});
		}else if(result.length == 0){
			var rating = {
				userId: parseInt(req.body.user_id),
				songId: parseInt(req.body.song_id),
				rating: parseInt(req.body.rating)
			};

			Rating.create(rating, function(err, play){
				if(err){
					res.json({
						status:'error',
						message: err,  
					});	
				}else{
					res.json({
						status:'success',
						message: 'Rated',  
					});
				}
			})
		}else if(result.length > 0){
			Rating.findOneAndUpdate({'userId':req.body.user_id, 'songId': req.body.song_id}, {$set: { rating: parseInt(req.body.rating) }}, function(err, result){
				if(err){
					res.json({
						status:'error',
						message: err,  
					});
				}else{
					res.json({
						status:'success',
						message: 'Updated',  
					});	
				}
			});
		}
	})	
}

module.exports.createuser = function(req, res, next){
	if(req.body.name && req.body.age && req.body.email && req.body.password && req.body.country){
		Sequence.findOneAndUpdate({'reference': 'users'}, {$inc:{count: 1}}, {new: true}, function(err, result){
	   		console.log(result.count)
	   		var user = {
				id: result.count,
				name: req.body.name,
				age: req.body.age,
				email: req.body.email,
				password: req.body.password,
				country: req.body.country
			}

			User.create(user, function(err, result1){
				res.json({
					message: result1
				})
			})
	   	})	
	}else{
		res.json({
			status: 'error',
			message: 'Some fields are missing!'
		})
	}
}

module.exports.login = function(req, res, next){
	if(req.body.email && req.body.password){
		User.findOne({'email': req.body.email, 'password': req.body.password},function(err,result){
			if(err){
				res.json({
					status: 'error',
					message: err
				})		
			}else if(result){
				res.json({
					status: 'success',
					message: result
				})
			}else{
				res.json({
					status: 'error',
					message: 'No such username password combination exist!'
				})
			}
		})
	}else{
		res.json({
			status: 'error',
			message: 'Some fields are missing!'
		})
	}
}

module.exports.madeForYou = function(req, res, next){
	if(req.query.userid){
		request('http://127.0.0.1:5000/madeforyou/'+req.query.userid, function (error, response, body) {
			//res.send(response.body);
			var jsonObject = JSON.parse(response.body);
			console.log(jsonObject)
			Song.find({'id': {$in: jsonObject}}, function(err, result){
				console.log(result)
				res.json({
					message: result
				})
			})
		});	
	}else{
		res.json({
			status: 'error',
			message: 'user id is missing'
		})
	}
	
}