var express = require('express');
var router = express.Router();
var apiRoutes = require('./api');

var initRoutes = function(app) {
	console.log("Initializing Routes...");
	app.use('/api', apiRoutes);
	console.log('Finished Initializing Routes...');
}

module.exports = initRoutes;