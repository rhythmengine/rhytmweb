var express = require('express');
var router = express.Router();
apicontroller = require('../controllers/apicontroller');

router.get('/getsongs', apicontroller.getSongs);
//GET PARAMS :- ?keyword=<search mai daalega vo>

router.get('/similarsongs', apicontroller.similarSongs); //ml server bhi lagega, mat use kar abhi
router.get('/madeforyou', apicontroller.madeForYou); //ml server bhi lagega, mat use kar abhi

router.get('/songsfromalbum', apicontroller.songsFromAlbum);
router.get('/songsfromartist',apicontroller.songsFromArtist);

router.post('/songplayed',apicontroller.songPlayed);
//POST PARAMS:- song_id (_id), user_id (_id)

router.post('/rate',apicontroller.rate);
//POST PARAMS:- song_id (id), user_id (id), rating.

router.get('/recentartist',apicontroller.recentArtist);	
router.get('/recentalbum',apicontroller.recentAlbum); 

router.post('/signup',apicontroller.createuser);
//POST PARAMS:- name, age, email, password, country

router.post('/login',apicontroller.login);
//POST PARAMS:- name, age, email, password, country

module.exports = router;